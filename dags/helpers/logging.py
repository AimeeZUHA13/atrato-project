import logging

logging.basicConfig()
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("forecast-catalog")
logger.setLevel(logging.INFO)


def get_logger():
    return logger
