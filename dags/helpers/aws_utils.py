import os
import io
import json
from boto3 import client as boto_client


class S3:
    def __init__(self):
        self.env = os.getenv("env")
        self.s3client = boto_client("s3")

    def save_to_parquet(self, df, bucket, key_out):

        buffer = io.BytesIO()
        df.to_parquet(buffer, compression="gzip",
                      engine="pyarrow", index=False)
        self.s3client.put_object(
            Body=buffer.getvalue(),
            Bucket=bucket,
            Key=key_out,
        )


class Redshift:
    @staticmethod
    def insert_data(schema, table, bucket, key, role, account, engine):

        with engine.begin() as conn:
            try:
                conn.execute(
                    f"""
                    BEGIN;
                        COPY {schema}.{table}
                        FROM 's3://{bucket}/{key}'
                        IAM_ROLE 'arn:aws:iam::{account}:role/{role}'
                        FORMAT AS PARQUET;
                    END;
                """
                )
            except Exception as e:
                print(f"Redshift Error: {e}")
                conn.execute("ROLLBACK;")
            finally:
                conn.close()


class ParameterStore:
    def __init__(self):
        self.ssm_client = boto_client("ssm")

    def get_parameter(self, parameter, env="dev"):
        response = self.ssm_client.get_parameter(
            Name=f"/atrato/{env}/{parameter}")
        return json.loads(response["Parameter"]["Value"])
