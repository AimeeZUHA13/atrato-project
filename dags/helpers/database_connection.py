from sqlalchemy import create_engine


def get_db_engine(params, rdbms="postgresql"):
    host = params.get(f"host")
    user = params.get(f"user")
    password = params.get(f"password")
    port = params.get("port")
    database = params.get("database")

    return create_engine(f"{rdbms}://{user}:{password}@{host}:{port}/{database}")
