from airflow import DAG
from datetime import datetime, timedelta
from airflow import DAG
from airflow.decorators import task
from airflow.utils.helpers import chain
from airflow.operators.empty import EmptyOperator
from libs.webservice import webservice_handler
from libs.data_handler import process_data

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2023, 8, 25),
    "email": ["aimee.@company.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5),
}

with DAG(
    "dag_forecast",
    default_args=default_args,
    catchup=False,
    schedule_interval=timedelta(hours=5),
) as dag:
    start = EmptyOperator()

    @task(task_id="extract_data", retries=1)
    def get_webserver():
        webservice_handler()

    _extract_data = get_webserver()

    @task(task_id="transform_and_load", retries=1)
    def transform_load():
        process_data()

    _transform_and_load = transform_load()

    chain(_extract_data, _transform_and_load)
