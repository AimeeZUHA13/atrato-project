import json
import gzip
import os
import pytz
import uuid
import datetime as dt
import pandas as pd
from helpers.logging import get_logger
from helpers.aws_utils import S3, Redshift, ParameterStore
from helpers.database_connection import get_db_engine

logger = get_logger()
today = dt.datetime.now(pytz.timezone(
    "America/Mexico_City")).strftime("%Y%m%d")
time = dt.datetime.now().strftime("%H%M%S")
id_uuid = str(uuid.uuid4())
env = os.getenv("env")
parameters = ParameterStore().get_parameter("atrato-dev", env)
engine = get_db_engine(json.loads(parameters))


def process_data():
    logger.info("Processing webservice data")
    forecast_data = read_file()
    for dict in forecast_data:
        path = f"dags/resources/raw/{dict['nes']}/{today}/{time}/"
        exist = os.path.exists(path)
        if not exist:
            os.makedirs(path)
            df = pd.DataFrame([dict])
            df = calculate_avg_data(df)
            key = f"conagua/{today}/{id_uuid}-forecast.gz.parquet"
            logger.info(f"Loading data to s3 path: {key}")
            S3().save_to_parquet(df, "bck-dev-atrato", key)
            logger.info("Loading data to Redshift.")
            Redshift().insert_data(
                "staging",
                "forecast",
                "bck-dev-atrato",
                key,
                "DataRedshift",
                123456789102,
                engine
            )


def read_file():
    with gzip.open("DailyForecast_MX", "rt", encoding="UTF-8") as zipfile:
        forecast_data = json.load(zipfile)
        return forecast_data


def calculate_avg_data(df):
    df_avg = pd.DataFrame()

    df_avg["ides"] = df["ides"].values
    df_avg["idmun"] = df["idmun"].values
    df_avg["nes"] = df["nes"].values
    df_avg["nmun"] = df["nmun"].values
    df_avg["tmax_avg"] = df["tmax"].mean()
    df_avg["prec_avg"] = df["prec"].mean()
    df_avg["extracted_at"] = today

    return df_avg
