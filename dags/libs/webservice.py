import requests
from requests.exceptions import HTTPError
from helpers.logging import get_logger

logger = get_logger()

ENDPOINT = "https://smn.conagua.gob.mx/webservices/index.php?method=1"
HEADERS = {"User-Agent": "Custom"}


def webservice_handler():
    response = get_connection()
    save_file(response)


def get_connection():
    try:
        session_obj = requests.Session()
        response = session_obj.get(ENDPOINT, headers=HEADERS)
        response.raise_for_status()
    except HTTPError as http_e:
        logger.info(f"An HTTP error occurred: {http_e}")
    except Exception as e:
        logger.info(f"Other error occurred: {e}")
    else:
        logger.info("Successful webservice connection")
        return response


def save_file(response):
    with open("DailyForecast_MX", "wb") as file:
        file.write(response.content)
        logger.info("Successful saving forecast file")
