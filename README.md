# Atrato Project

## 1. SQL exercise

Queries used:

1. Create the table:

```sql
CREATE TABLE  STUDENT (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  score FLOAT NOT NULL
);
```

2. Load the table

```sql
INSERT INTO STUDENT VALUES ('Bob',50);
INSERT INTO STUDENT VALUES ('John', 65.5);
INSERT INTO STUDENT VALUES ('Harry',45);
INSERT INTO STUDENT VALUES ('Edgar',85);
INSERT INTO STUDENT VALUES ('Dev',25);
INSERT INTO STUDENT VALUES ('Sid',98);
INSERT INTO STUDENT VALUES ('Tom',90);
INSERT INTO STUDENT VALUES ('Julia',70.5);
INSERT INTO STUDENT VALUES ('Erica',81);
INSERT INTO STUDENT VALUES ('Jerry',85);
```

3. Query to get the NAMEs in descending order by SCORE, then ascending order by ID for matching SCOREs.

```sql
SELECT 
  id, 
  name 
FROM 
  STUDENT 
ORDER BY 
  score desc, 
  id asc;
```

4. Query output

![Alt text](sql_output.png)

## 2. Questions

1. How would you propose to keep track of the tables that are breaking in order to provide you teammates the latest information?

First of all I would check to see if the records that are showing that inconsistency have a date who can help me to know if that records are the most recent updated or inserted, if that date does not exist then I would create a CDC type pipeline to monitor each source to get the most recent records and then I would upload them into a centralized system(relational database, NoSQL, S3, etc) to be able to provide a reliable data.

2. What should we do to centralize the data in order to display it in charts for KPI monitoring? What would you propose the data governance strategy should be?

At the end, that data is transactional so first of all we need to extract and load into another system(in a low key mode) so that this system can only be consulted by the data visualization platform. Once the extraction is ready and the data is in a "intermediate" storage we need to make the necessary transformation and matches between the multiple data sources to provide an OLAP kind of system where the final tables are the ones that represent the KPIs.

Speaking of data governance strategy, we need to make sure the data is always available, to ensure this we need to make scalables and reliables pipelines. We need to ensure only certain people can consult the data that they need and no more, to make sure of this, we need a system that can act as a shield.

### 3. Airflow pipeline

I use an Airflow installation on Docker. The data that I consult was from Conagua where the climatological data by state is sent via a webserver, this data is updated everyday so the data for previous days is always deleted and the data is send in a kind of raw format i.e. we need to make some cleaning, that was the first and second challenge that we need to solve, for the first one we need to upload the data into an storage system to have the historical data(I use S3) and for the second challenge I perform some calculations on the data to obtain information that can be analyzed and then upload that data into Redshift.